/*
硬件版本是4.12，程序里在文件conf_general.h里用#define HW_VERSION_410定义，驱动是DRV8302
编译时会在conf_general.h里查看定义了哪个宏从而编译出对应的bin文件

如果想编译出别的版本的bin文件，用make -j8 build_args="-DCURRENT_SHUNT_RES=0.0005 -DHW_VERSION_46"命令
传递编译参数，这样尽管conf_general.h里是定义了HW_VERSION_410宏，但编译时会以HW_VERSION_46为准，同时
宏CURRENT_SHUNT_RES也会是0.0005，而不是文件hw_46.h里的0.001
即build_args="-DCURRENT_SHUNT_RES=0.0005 -DHW_VERSION_46"会在编译前定义了宏HW_VERSION_46，这样在
文件conf_general.h里就不会满足#if !defined(HW_VERSION_40)这一系列条件，这样文件conf_general.h里就不会执行
#define HW_VERSION_410 这一语句，同样，因为编译前定义了CURRENT_SHUNT_RES=0.0005，所以在hw_46.h里也不会
满足#ifndef CURRENT_SHUNT_RES，即也不会执行#define CURRENT_SHUNT_RES		0.001语句，这样，
宏CURRENT_SHUNT_RES就是0.0005了，即make -D参数因提前定义了宏，使得文件里的宏因不满足条件，文件里的宏定义失去作用

编译出来的bin文件在build文件夹里，叫做BLDC_4_ChibiOS.bin。而我们要想编译所有版本的bin文件，则进入build_all文件夹
执行./rebuild_all(如果执行有错误，将这个文件从dos格式改成unix格式)。在rebuild_all文件里可发现先更新conf_general.h文件，
然后编译时传递参数编译出对应版本的bin文件，这时的bin文件默认为BLDC_4_ChibiOS.bin，再改名为对应版本的bin文件，如VESC_33k.bin
BLDC_4_ChibiOS.bin这个名字是由Makefile里第93行PROJECT = BLDC_4_ChibiOS决定的，然后在264行里
build/$(PROJECT).bin: build/$(PROJECT).elf 将elf改成bin文件，各个C文件编译链接生成elf文件

如果一个函数有多个定义，不知道是用了哪个定义，可看make记录，查看是哪个C文件被编译

ST_ENABLE_CLOCK();是在st_lld.c里根据STM32_ST_USE_TIMER定义，而STM32_ST_USE_TIMER在两个名字相的头文件里定义，根据编译
记录，对应的两个目录都参与了编译，但在st_lld.c前面发现包含了#include "hal.h"，打开hal.h又发现#include "halconf.h"，在
halconf.h里发现#include "mcuconf.h"，而mcuconf.h有STM32_ST_USE_TIMER定义，所以可以确定STM32_ST_USE_TIMER定义是在mcuconf.h
里
do_dc_cal();//这个函数没有在头文件里声明，说明这个函数只能在本文件中使用，即是mcpwm.c里的do_dc_cal函数

程序从0x8000000开始运行，在0x8080000-0x80e0000处共384KB用来保存新APP程序，0x80e0000到最后面共128KB字节存放bootloader程序。
上电后程序从0x8000000开始运行APP程序，如果升级时会先接收新应该程序的数据并保存在0x8080000-0x80e0000处(可以有中断)，然后关闭操作系
统，并闭中断，跳到0x80e0000 bootloader处，bootloader程序会从0x8080000-0x80e0000读出数据并写到0x8000000开始的384KB，然后利用
看门狗重新启动MCU
如果在写新APP程序途中断电，然后再重新上电，程序不能从0x8000000处启动，造成芯片变砖，只能用stlink来重新烧录程序了
0x8004000-0x8008000为电调参数存放区，0x8008000-0x800c000为APP参数存放区



mcpwm_adc_int_handler是DMA中断函数


filter_create_fir_lowpass低通滤波，牵涉到FFT变换及汉明窗，以后有空再看
app_set_configuration未看懂，有空再看，里面创建了线程




chThdGetSelfX()取得当前线程ID
chEvtSignal()发送事件
chEvtWaitAny()等待事件


commands_process_packet(data, len); //根据不同上位机命令作不同处理，这个函数重点看
mc_interface_set_brake_current





*/
